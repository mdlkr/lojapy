def obter_limite():

    print("Antes de iniciar suas compras, faremos uma análise de crédito, para isso precisaremos de algumas informações suas, ok?\nVamos lá!\n")
    print("------------------------------------------------------------------------------\n")
    nome = input("Qual é o seu primeiro nome? ")
    cargo = input("Qual é o seu cargo na empresa em que trabalha? ")
    salario = float(input("Qual é o seu salário? "))
    anoNascimento = int(input("Qual e o seu ano de nascimento? "))
    
    print("\n--")
    print("Seu nome é: {};".format(nome))
    print("Seu cargo é {};".format(cargo))
    print("Seu salário é R$ {:.2f};".format(salario))
    print("Você nasceu em {}.".format(anoNascimento))
    print("--\n")
    print("Show!\n")

    idadeAprox = 2021 -(anoNascimento)
    limiteGasto = float((salario * (idadeAprox / 1000)) + 100)
    
    print ("Sua idade aproximada é {} anos, correto?!".format(idadeAprox))
    print ("Informamos que seu limite de gasto em nossa loja é de R${:.2f}. Boas compras!\n".format(limiteGasto))
    print("------------------------------------------------------------------------------\n")

    return (limiteGasto,nome,idadeAprox)

def verificar_produto(limiteGasto,valorAcumulado):

    if valorAcumulado <= (limiteGasto * 0.6):
        print("Sua compra está liberada!")
        print("\n------------------------------------------------------------------------------\n")

    elif (limiteGasto * 0.6) < valorAcumulado < (limiteGasto * 0.9):
        print("Sua compra está liberada, poderá parcelar em até 2 vezes.")
        print("\n------------------------------------------------------------------------------\n")

    elif (limiteGasto * 0.9) <= valorAcumulado <= limiteGasto:
        print("Sua compra está liberada, poderá parcelar em 3 ou mais vezes.")
        print("\n------------------------------------------------------------------------------\n")

    else:
        print("Este produto está bloqueado para compra devido ao valor de seu limite de gasto, desculpe.")
        print("\n------------------------------------------------------------------------------\n")

    return (limiteGasto,valorAcumulado)


print("Olá! Está é a Loja da Quinta, seja muito bem-vindo!\n")
print("------------------------------------------------------------------------------\n")

(limiteGasto,nome,idadeAprox) = obter_limite()

n_produtos = int(input("Quantos produtos deseja cadastrar? "))
print("\n------------------------------------------------------------------------------\n")

valorAcumulado = 0
saldo = limiteGasto

for i in range(n_produtos):
    
    nomeProduto = input("Por favor, digite o nome do produto que deseja comprar: ")
    valorProduto = float(input("Agora digite seu respectivo valor (em reais): "))
    print("------------------------------------------------------------------------------\n")    

    valorAcumulado += valorProduto
    saldo -= valorProduto

if  valorAcumulado <= limiteGasto: 
    print("O valor total é de R$ {:.2f}.".format(valorAcumulado))
    print("Seu saldo é de R$ {:.2f}.\n".format(saldo))
    print("------------------------------------------------------------------------------\n")
    print("Obrigada por escolher nossa loja!\n")
    print("------------------------------------------------------------------------------\n")
    
else:
    print("Desculpe, este valor ultrapassa seu limite.\n")
    print("\n------------------------------------------------------------------------------\n")
    
limiteGasto,valorAcumulado = verificar_produto(limiteGasto,valorAcumulado)

for i in range(valorAcumulado <= (limiteGasto * 0.6) or (limiteGasto * 0.6) < valorAcumulado < (limiteGasto * 0.9) or (limiteGasto * 0.9) <= valorAcumulado <= limiteGasto):
    nomeCompleto = input("Agora que você já escolheu seu produto, precisarei saber seu nome completo para calcularmos um descontinho!\nConta para mim? ")
    print("\n------------------------------------------------------------------------------\n")
    desconto = float(len(nome))
    if int(len(nomeCompleto))<= valorAcumulado >= idadeAprox:
        print("Você terá um desconto de R$ {}".format(desconto))
        valorFinal = valorAcumulado - desconto
        print("O valor final de seu produto é de R$ {:.2f}".format(valorFinal))
    else:
        print("Infelizmente não terá desconto.")
    break

print("\nObrigada por comprar com a gente, volte sempre! :)")
